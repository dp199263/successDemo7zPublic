<?php
declare(strict_type=1);


class Request {
	public $uri;
	public $method;
	public $message;
	
	function __construct() {
		
   	}
}

class Response {
	public $message;
	function __construct() {
		
   	}
}


class IndexController {
	public function AAction(&$req, &$resp) {
		//$resp->message = "this is from " .  get_class($this) . _FUNCTION__;
		$resp->message = sprintf("controller name = %s,  methodname= %s ", get_class($this) , __FUNCTION__);
	}
}


function getControllerName($str, &$controllerName, &$methodName) {
	$arr = explode("/", $str);
	$controllerName = "ErrorController";
	$methodName = "IndexAction";
	
	//指定了controller
	if(count($arr) >= 2) {
		if(!empty($arr[1])) {
			$controllerName = ucfirst($arr[1])."Controller";
		}

		if(count($arr) >= 3) {
			if(!empty($arr[2])) {
				$methodName = ucfirst($arr[2])."Action";
			}
		}
	}
}



$str="/index/a";
getControllerName($str, $controllerName, $methodName);
var_dump($controllerName);
var_dump($methodName);

$req = new Request();
$resp = new Response();
$controller = new $controllerName;
$controller->$methodName($req, $resp);

var_dump($resp);


/**
$req = new Request();
$resp = new Response();
(new IndexController())->a($req, $resp);

var_dump($resp);
*/




