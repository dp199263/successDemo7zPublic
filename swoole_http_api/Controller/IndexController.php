<?php

class IndexController {
	public function AAction(&$req, &$resp) {
		//$resp->message = "this is from " .  get_class($this) . _FUNCTION__;
		$str = sprintf("controller name = %s,  methodname= %s ", get_class($this) , __FUNCTION__);
		$resp->header("Content-Type", "application/json");
		$resp->header("AAA", "2333");
		$ret = ["msg" => $str];
		$resp->end(json_encode($ret));
	}	

	public function QuerymysqlAction(&$req, &$resp) {
		$str = sprintf("controller name = %s,  methodname= %s ", get_class($this) , __FUNCTION__);
		$resp->header("Content-Type", "application/json");
		$resp->header("AAA", "2333");
		$students = [];
		
		$dsn = "mysql:host=127.0.0.1;dbname=t"; 
		$dbh = new PDO($dsn, 'root', '111222'); 
		$sql = "SELECT * from student;"; 
		$sth = $dbh->prepare($sql); 
		$sth->execute(); 
		$result = $sth->fetchAll(PDO::FETCH_ASSOC); 
		// print_r($result);

		$dbh = NULL;
		$ret = ["msg" => $str, "data" => $result];
		$resp->end(json_encode($ret));
	}


    public function TestinsertAction(&$req, &$resp) {
        go(function() {
            $swoole_mysql = new Swoole\Coroutine\MySQL();
            $swoole_mysql->connect([
                'host' => '127.0.0.1',
                'port' => 3306,
                'user' => 'root',
                'password' => '111222',
                'database' => 't',
                'charset' => 'utf8', //指定字符集
                'timeout' => 2,  // 可选：连接超时时间（非查询超时时间），默认为SW_MYSQL_CONNECT_TIMEOUT（1.0）
            ]);
            $sql = "select * from test";
            $res = $swoole_mysql->query($sql);

            if($res === false) {
                return;
            }
            foreach ($res as $value) {
                print_r($value);
            }
        });


        $ret = ["msg" => $str, "data" => $res];
        $resp->end(json_encode($ret));
    }

}