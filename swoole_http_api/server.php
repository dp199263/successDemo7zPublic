<?php


require "Controller/IndexController.php";
require "Controller/ErrorController.php";


function getControllerMethodName($str, &$controllerName, &$methodName) {
	$arr = explode("/", $str);
	$controllerName = "ErrorController";
	$methodName = "IndexAction";
	
	//指定了controller
	if(count($arr) >= 2) {
		if(!empty($arr[1])) {
			$controllerName = ucfirst($arr[1])."Controller";
		}

		if(count($arr) >= 3) {
			if(!empty($arr[2])) {
				$methodName = ucfirst($arr[2])."Action";
			}
		}
	}
}

$http = new swoole_http_server("127.0.0.1", 9501);
$http->set(array(
    'worker_num' => 12,
    // 'daemonize' => true,
    'backlog' => 128,
));


$http->on("start", function ($server) {
    echo "Swoole http server is started at http://127.0.0.1:9501 \n";
});

$http->on("request", function ($request, $response) {
	// var_dump($request);	
	$controllerName = "";
	$methodName = "";
	// var_dump($request->server['request_uri']);


	//TODO 生成一个扫描controller目录，所有文件的名字、得到全局allControllers的list跟allControllerMethodMap列表, 根据uri判断是不是在里面。错误直接error
    //TODO 正则判断只放过部分请求过来，
	getControllerMethodName($request->server['request_uri'], $controllerName, $methodName);

	// var_dump($controllerName);
	// var_dump($methodName);
	$controller = new $controllerName;
	//这里已经设置end发送str了
	$controller->$methodName($request, $response);

    // $response->header("Content-Type", "text/plain");
    // $response->end("Hello World\n");
});

$http->start();