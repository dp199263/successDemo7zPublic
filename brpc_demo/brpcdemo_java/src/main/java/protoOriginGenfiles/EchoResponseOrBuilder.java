// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: echo.proto

package protoOriginGenfiles;

public interface EchoResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:example.EchoResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>required string message = 1;</code>
   */
  boolean hasMessage();
  /**
   * <code>required string message = 1;</code>
   */
  java.lang.String getMessage();
  /**
   * <code>required string message = 1;</code>
   */
  com.google.protobuf.ByteString
      getMessageBytes();
}
