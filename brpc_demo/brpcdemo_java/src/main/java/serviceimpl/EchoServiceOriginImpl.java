package serviceimpl;

import brpcServiceInterfaces.EchoService;
import protoOriginGenfiles.EchoRequest;
import protoOriginGenfiles.EchoResponse;

public class EchoServiceOriginImpl implements EchoService {
    @Override
    public EchoResponse echo(EchoRequest request) {
        EchoResponse response = EchoResponse.newBuilder().setMessage("this is from brpc java genfiles 你好" + request.getMessage()).build();
        return response;
    }
}