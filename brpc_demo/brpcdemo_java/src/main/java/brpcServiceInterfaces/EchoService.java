package brpcServiceInterfaces;

import com.baidu.brpc.protocol.BrpcMeta;
import protoOriginGenfiles.EchoRequest;
import protoOriginGenfiles.EchoResponse;

public interface EchoService {
    @BrpcMeta(serviceName = "example.EchoService", methodName = "Echo")
    EchoResponse echo(EchoRequest request);
}
