import com.baidu.brpc.server.RpcServer;
import com.baidu.brpc.server.RpcServerOptions;
import serviceimpl.EchoServiceOriginImpl;

public class BrpcServerOriginProto {
    public static void main(String[] args) {
        int port = 1032;
        if (args.length == 1) {
            port = Integer.valueOf(args[0]);
        }

        RpcServerOptions options = new RpcServerOptions();
        options.setReceiveBufferSize(64 * 1024 * 1024);
        options.setSendBufferSize(64 * 1024 * 1024);

        options.setKeepAliveTime(20);
//        options.setNamingServiceUrl("zookeeper://127.0.0.1:2181");
//        final RpcServer rpcServer = new RpcServer(port, options);
        final RpcServer rpcServer = new RpcServer(port, options);
        rpcServer.registerService(new EchoServiceOriginImpl());

        rpcServer.start();

        // make server keep running
        synchronized (BrpcServerOriginProto.class) {
            try {
                BrpcServerOriginProto.class.wait();
            } catch (Throwable e) {
            }
        }
    }
}
