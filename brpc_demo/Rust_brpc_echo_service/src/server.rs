use brpc_rs::{Server, ServerOptions, ServiceOwnership};
use std::borrow::Borrow;

pub mod echo {
    include!(concat!(env!("OUT_DIR"), "/example.rs"));
    include!(concat!(env!("OUT_DIR"), "/example.brpc.rs"));
}

fn main() {
    let mut service = echo::EchoService::new();
    service.set_echo_handler(&mut move |request, mut response| {
        response.message = request.message.clone();
        Ok(())
    });

    let mut server = Server::new();
    let mut options = ServerOptions::new();
    options.set_idle_timeout_ms(1000);
    server
        .add_service(&service, ServiceOwnership::ServerDoesntOwnService)
        .expect("Failed to add service");
    server.start(50000, &options).expect("Failed to start service");
    server.run(); // Run until CTRL-C
}
