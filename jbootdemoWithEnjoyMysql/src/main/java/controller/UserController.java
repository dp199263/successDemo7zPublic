package controller;

import com.dplord.services.UserService;
import com.jfinal.aop.Inject;
import io.jboot.web.controller.JbootController;
import io.jboot.web.controller.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/user")
public class UserController extends JbootController {

    @Inject
    private UserService userService;

    public void index() {
        //向前台传入list
        List<String> list = new ArrayList<>();
        list.add("张三");
        list.add("李四");
        list.add("王二");
        setAttr("names", list);
        render("index.html");
    }

    public void all() {
        //向前台传入list
        setAttr("userList", userService.findAll());
        render("all.html");
    }
}
