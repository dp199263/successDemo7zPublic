package controller;

import com.dplord.models.User;
import com.dplord.services.UserService;
import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import io.jboot.utils.HttpUtil;
import io.jboot.web.controller.JbootController;
import io.jboot.web.controller.annotation.RequestMapping;
import java.util.List;

@RequestMapping("/")
public class IndexController extends JbootController {

    public void index() {
        String a = "mihao是捏";
        String html = HttpUtil.httpGet("http://cs.hust.edu.cn");
        renderText("Hello World Jboot" + html);
    }

    public void index2() {
        List<Record> records = Db.find("select * from user");
//        renderText(Arrays.toString(records.toArray()));
        renderJson(records);
    }

    @Inject
    private UserService userService;

    public void index3() {
        List<User> users = userService.findAll();
        renderJson(users);
    }
}