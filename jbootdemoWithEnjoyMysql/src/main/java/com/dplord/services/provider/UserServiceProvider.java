package com.dplord.services.provider;

import io.jboot.aop.annotation.Bean;
import com.dplord.services.UserService;
import com.dplord.models.User;
import io.jboot.service.JbootServiceBase;


@Bean
public class UserServiceProvider extends JbootServiceBase<User> implements UserService {

}