package config;

import com.jfinal.config.Routes;
import com.jfinal.template.Engine;
import com.jfinal.template.source.ClassPathSourceFactory;
import controller.IndexController;
import io.jboot.core.listener.JbootAppListenerBase;


/**
 * jfinal config
 * @author Rlax
 *
 */
public class JfinalConfigListener extends JbootAppListenerBase {
    @Override
    public void onEngineConfig(Engine engine) {
        super.onEngineConfig(engine);
        engine.setSourceFactory(new ClassPathSourceFactory());
        engine.setBaseTemplatePath("/template");
    }

/*
    设置首页
    https://www.oschina.net/question/2008333_183329
 */
    @Override
    public void onRouteConfig(Routes routes) {
        super.onRouteConfig(routes);
        routes.add("/", IndexController.class);
    }
}