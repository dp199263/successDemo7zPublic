import io.jboot.app.JbootApplication;

public class Application {
    public static void main(String[] args) {
        JbootApplication.run(args);
    }
}
